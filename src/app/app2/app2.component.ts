import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-app2',
  templateUrl: './app2.component.html',
  styleUrls: ['./app2.component.css']
})
export class App2Component implements OnInit {

  a: number;
  b: String;
  c: any;

  constructor(private router:Router) { }

  ngOnInit(): void {
  }


  m1() {
    this.router.navigate(['comp1']);
  }

}
