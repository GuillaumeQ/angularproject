import { Component, OnInit } from '@angular/core';
import { Article } from '../article';

@Component({
  selector: 'app-tp-articlev1',
  templateUrl: './tp-articlev1.component.html',
  styleUrls: ['./tp-articlev1.component.css']
})
export class TpArticlev1Component implements OnInit {

  article1: {123, "Voiture", 20000};
  a:Article = new Article();
  articleTab: Array<Article> = new Array<Article>();
  constructor() { }

  ngOnInit(): void {
  }

  create(){
    let copy:Article = new Article();
    copy.id = this.a.id;
    copy.marque = this.a.marque;
    copy.prix = this.a.prix;
    this.articleTab.push(copy);
    console.log(this.articleTab);
  }
}
