import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TpArticlev1Component } from './tp-articlev1.component';

describe('TpArticlev1Component', () => {
  let component: TpArticlev1Component;
  let fixture: ComponentFixture<TpArticlev1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TpArticlev1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TpArticlev1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
