import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppItemComponent } from './app-item/app-item.component';
import { AppPanierComponent } from './app-panier/app-panier.component';
import { AppPersonneComponent } from './app-personne/app-personne.component';
import { App1Component } from './app1/app1.component';
import { App10Component } from './app10/app10.component';
import { App2Component } from './app2/app2.component';
import { App3Component } from './app3/app3.component';
import { App4Component } from './app4/app4.component';
import { App5Component } from './app5/app5.component';
import { App6Component } from './app6/app6.component';
import { App7Component } from './app7/app7.component';
import { App8Component } from './app8/app8.component';
import { App9Component } from './app9/app9.component';
import { CalculetteComponent } from './calculette/calculette.component';
import { Comp1Component } from './comp1/comp1.component';
import { Comp2Component } from './comp2/comp2.component';
import { CompbsComponent } from './compbs/compbs.component';
import { GuessnumberComponent } from './guessnumber/guessnumber.component';
import { Personnesession1Component } from './personnesession1/personnesession1.component';
import { Personnesession2Component } from './personnesession2/personnesession2.component';
import { Session1Component } from './session1/session1.component';
import { Session2Component } from './session2/session2.component';
import { TpArticlev1Component } from './tp-articlev1/tp-articlev1.component';

const routes: Routes = [
  { path: 'calculette', component: CalculetteComponent },
  { path: 'guessnumber', component: GuessnumberComponent },
  { path: 'tpArticlev1', component: TpArticlev1Component },
  { path: 'app1', component: App1Component },
  { path: 'app2', component: App2Component },
  { path: 'app3', component: App3Component },
  { path: 'app4', component: App4Component },
  { path: 'app5', component: App5Component },
  { path: 'app6', component: App6Component },
  { path: 'app7', component: App7Component },
  { path: 'app8', component: App8Component },
  { path: 'app9', component: App9Component },
  { path: 'app10', component: App10Component },
  { path: 'session1', component: Session1Component },
  { path: 'session2', component: Session2Component },
  { path: 'personnesession1', component: Personnesession1Component },
  { path: 'personnesession2', component: Personnesession2Component },
  { path: 'app-personne', component: AppPersonneComponent },
  { path: 'app-item', component: AppItemComponent },
  { path: 'app-panier', component: AppPanierComponent },

  { path: 'comp1', component: Comp1Component },
  { path: 'comp2', component: Comp2Component },
  { path: 'compbs', component: CompbsComponent },
  { path: '', redirectTo: "/compbs", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
