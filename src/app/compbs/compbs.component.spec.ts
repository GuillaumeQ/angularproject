import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompbsComponent } from './compbs.component';

describe('CompbsComponent', () => {
  let component: CompbsComponent;
  let fixture: ComponentFixture<CompbsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompbsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
