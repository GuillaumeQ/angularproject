import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app7',
  templateUrl: './app7.component.html',
  styleUrls: ['./app7.component.css']
})
export class App7Component implements OnInit {

  info:String;
  strTab:Array<String> = new Array<String>();
  // strTab=[];
  constructor() { }

  ngOnInit(): void {
  }
  add(){
    this.strTab.push(this.info);
  }

}
