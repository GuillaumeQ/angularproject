import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-session2',
  templateUrl: './session2.component.html',
  styleUrls: ['./session2.component.css']
})
export class Session2Component implements OnInit {

  message:String;
  constructor() { }

  ngOnInit(): void {
    this.message = sessionStorage.getItem("toto");
  }

}
