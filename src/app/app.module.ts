import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Comp1Component } from './comp1/comp1.component';
import { Comp2Component } from './comp2/comp2.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { Comp1aComponent } from './comp1a/comp1a.component';
import { Comp1bComponent } from './comp1b/comp1b.component';
import { CompbsComponent } from './compbs/compbs.component';
import { App1Component } from './app1/app1.component';
import { App2Component } from './app2/app2.component';
import { App3Component } from './app3/app3.component';
import { App4Component } from './app4/app4.component';
import { FormsModule } from '@angular/forms';
import { CalculetteComponent } from './calculette/calculette.component';
import { GuessnumberComponent } from './guessnumber/guessnumber.component';
import { App5Component } from './app5/app5.component';
import { App6Component } from './app6/app6.component';
import { App7Component } from './app7/app7.component';
import { App8Component } from './app8/app8.component';
import { App9Component } from './app9/app9.component';
import { App10Component } from './app10/app10.component';
import { TpArticlev1Component } from './tp-articlev1/tp-articlev1.component';
import { Session1Component } from './session1/session1.component';
import { Session2Component } from './session2/session2.component';
import { Personnesession1Component } from './personnesession1/personnesession1.component';
import { Personnesession2Component } from './personnesession2/personnesession2.component';
import { AppPersonneComponent } from './app-personne/app-personne.component';
import { AppItemComponent } from './app-item/app-item.component';
import { AppPanierComponent } from './app-panier/app-panier.component';

@NgModule({
  declarations: [
    AppComponent,
    Comp1Component,
    Comp2Component,
    HeaderComponent,
    FooterComponent,
    Comp1aComponent,
    Comp1bComponent,
    CompbsComponent,
    App1Component,
    App2Component,
    App3Component,
    App4Component,
    CalculetteComponent,
    GuessnumberComponent,
    App5Component,
    App6Component,
    App7Component,
    App8Component,
    App9Component,
    App10Component,
    TpArticlev1Component,
    Session1Component,
    Session2Component,
    Personnesession1Component,
    Personnesession2Component,
    AppPersonneComponent,
    AppItemComponent,
    AppPanierComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
