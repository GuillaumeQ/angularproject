import { Component, OnInit } from '@angular/core';
import { Adresse } from '../adresse';
import { InfoPersonne } from '../info-personne';
import { Personne } from '../personne';

@Component({
  selector: 'app-app-personne',
  templateUrl: './app-personne.component.html',
  styleUrls: ['./app-personne.component.css']
})
export class AppPersonneComponent implements OnInit {

  message:String;
  constructor() { }

  ngOnInit(): void {
    let p : Personne = new Personne();

    p.id = 10;
    p.nom = "g";
    p.prenom = "q";
    p.age = 10;

    let a : Adresse = new Adresse();
    a.numero = 15;
    a.rue = "afezf";
    a.cp = "75324";

    let z: InfoPersonne = new InfoPersonne();
    z.secu = 12345;
    z.p = p;
    z.a = a;
    z.compute();
    this.message = z.info;
  }

}
