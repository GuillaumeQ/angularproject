import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPersonneComponent } from './app-personne.component';

describe('AppPersonneComponent', () => {
  let component: AppPersonneComponent;
  let fixture: ComponentFixture<AppPersonneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppPersonneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPersonneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
