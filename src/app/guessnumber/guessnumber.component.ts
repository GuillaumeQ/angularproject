import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-guessnumber',
  templateUrl: './guessnumber.component.html',
  styleUrls: ['./guessnumber.component.css']
})
export class GuessnumberComponent implements OnInit {
  randomNb: number;
  userNb: number;
  isWin: String = "";
  tryNb:number = 0;

  constructor() {
  }

  ngOnInit(): void {
    this.randomNb = Math.floor(Math.random() * 10);
  }

  guessNb(userNb:number) {
    this.tryNb++;
    this.isWin = (userNb == this.randomNb) ? "Gagné ! :)" : "Perdu ;)"; 
  }
}
