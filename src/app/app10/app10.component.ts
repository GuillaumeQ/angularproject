import { Component, OnInit } from '@angular/core';
import { Personne } from '../personne';

@Component({
  selector: 'app-app10',
  templateUrl: './app10.component.html',
  styleUrls: ['./app10.component.css']
})
export class App10Component implements OnInit {
p:Personne = new Personne();
  constructor() { }

  ngOnInit(): void {
  }
  
  create(){
    console.log(this.p);
  }
}
