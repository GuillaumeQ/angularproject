import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculette',
  templateUrl: './calculette.component.html',
  styleUrls: ['./calculette.component.css']
})
export class CalculetteComponent implements OnInit {

  nb1: number;
  nb2: number;
  result:number;

  constructor() { }

  ngOnInit(): void {
  }

  add() {
    this.result = this.nb1 + this.nb2;
  }

  sub() {
    this.result = this.nb1 - this.nb2;
  }

  mult() {
    this.result = this.nb1 * this.nb2;
  }

  div() {
    this.result = this.nb1 / this.nb2;
  }

}
