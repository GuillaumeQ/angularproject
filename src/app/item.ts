export class Item {
    marque: String;
    prix: number;
    constructor(a: String, b: number) {
        this.marque = a;
        this.prix = b;
    }

    getInfo(){
        return `${this.marque}  ${this.prix}`;
    }
}
