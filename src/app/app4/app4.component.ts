import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app4',
  templateUrl: './app4.component.html',
  styleUrls: ['./app4.component.css']
})
export class App4Component implements OnInit {

  a:number;
  message: number;
  constructor() { }

  ngOnInit(): void {
  }

  m1 () {
    this.message = this.a;
  }

}
