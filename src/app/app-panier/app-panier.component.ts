import { Component, OnInit } from '@angular/core';
import { Article } from '../article';
import { Ligne } from '../ligne';
import { Panier } from '../panier';

@Component({
  selector: 'app-app-panier',
  templateUrl: './app-panier.component.html',
  styleUrls: ['./app-panier.component.css']
})
export class AppPanierComponent implements OnInit {
  article:Article = new Article();
  ligne:Ligne = new Ligne();
  panier:Panier = new Panier();


  constructor() { }

  ngOnInit(): void {
  }

  addToList() {
    let articleCopy:Article = new Article();
    articleCopy.id = this.article.id;
    articleCopy.marque = this.article.marque;
    articleCopy.prix = this.article.prix;

    let ligneCopy: Ligne = new Ligne();
    ligneCopy.Article = articleCopy;
    ligneCopy.quantite = this.ligne.quantite;
    ligneCopy.prixLigne = articleCopy.prix * this.ligne.quantite;

    this.panier.listLigne.push(ligneCopy);
    this.panier.prixTotal = this.panier.listLigne.reduce((a, b) => a + b.prixLigne, 0);
    console.log(this.panier);
  }
}
