import { Component, OnInit } from '@angular/core';
import { Item } from '../item';

@Component({
  selector: 'app-app-item',
  templateUrl: './app-item.component.html',
  styleUrls: ['./app-item.component.css']
})
export class AppItemComponent implements OnInit {

  message:String;

  constructor() { }

  ngOnInit(): void {
  let i : Item = new Item("aa", 10);
  this.message = i.getInfo();
  }

}
