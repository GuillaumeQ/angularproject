import { Component, OnInit } from '@angular/core';
import { Personne } from '../personne';

@Component({
  selector: 'app-app9',
  templateUrl: './app9.component.html',
  styleUrls: ['./app9.component.css']
})
export class App9Component implements OnInit {

  id: number;
  nom: String;
  prenom: String;
  age: number;

  p: Personne = new Personne();
  constructor() { }

  ngOnInit(): void {
  }

  create() {
  this.p.id = this.id;
  this.p.nom = this.nom;
  this.p.prenom = this.prenom;
  this.p.age = this.age;
  console.log(this.p);
  }

  createv2() {
    let p2: Personne = new Personne();
    
    p2.id = this.id;
    p2.nom = this.nom;
    p2.prenom = this.prenom;
    p2.age = this.age;
    console.log(p2);
    }
}
