import { Component, OnInit } from '@angular/core';
import { Personne } from '../personne';

@Component({
  selector: 'app-personnesession1',
  templateUrl: './personnesession1.component.html',
  styleUrls: ['./personnesession1.component.css']
})
export class Personnesession1Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
  f() {
    let p: Personne = new Personne();
    p.nom = "Q";
    p.prenom = "G";
    p.id = 1;
    p.age = 20;

    let str:string = JSON.stringify(p);
    sessionStorage.setItem("p", str);
  }
}
